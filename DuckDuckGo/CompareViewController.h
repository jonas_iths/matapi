//
//  CompareViewController.h
//  DuckDuckGo
//
//  Created by Jonas on 2015-03-21.
//  Copyright (c) 2015 Jonas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GKBarGraph.h>

@interface CompareViewController : UIViewController <GKBarGraphDataSource>
@property (weak, nonatomic) NSString *foodThing1;
@property (weak, nonatomic) NSString *foodThing2;
@property (strong, nonatomic) NSMutableArray *array;

@end
