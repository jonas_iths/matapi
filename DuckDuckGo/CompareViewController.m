//
//  CompareViewController.m
//  DuckDuckGo
//
//  Created by Jonas on 2015-03-21.
//  Copyright (c) 2015 Jonas. All rights reserved.
//

#import "CompareViewController.h"
#import <GKBarGraph.h>


@interface CompareViewController ()

@property (strong, nonatomic) IBOutlet GKBarGraph *graph;
@property (weak, nonatomic) IBOutlet UILabel *food1;
@property (weak, nonatomic) IBOutlet UILabel *food2;

@end

@implementation CompareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.graph.dataSource = self;
    [self.graph draw];   
    
    self.food1.text = self.foodThing1;
    self.food2.text = self.foodThing2;
        
}

- (void)viewDidLayoutSubviews {
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
}

- (NSInteger)numberOfBars {
    return 4;
}

- (NSNumber *)valueForBarAtIndex:(NSInteger)index {
    
    switch (index) {
        case 0:
            return self.array[0];
        case 1:
            return self.array[2];
        case 2:
            return self.array[1];
        case 3:
            return self.array[3];
    }
    
    return self.array[index];
}


- (UIColor *)colorForBarAtIndex:(NSInteger)index {
    switch (index){
            
        case 0:
            return [UIColor redColor];
        case 1:
            return [UIColor blueColor];
        case 2:
            return [UIColor redColor];
        case 3:
            return [UIColor blueColor];
    }
    
    return [UIColor yellowColor];
    
}


- (UIColor *)colorForBarBackgroundAtIndex:(NSInteger)index {
    
    return [UIColor yellowColor];
    
}

- (CFTimeInterval)animationDurationForBarAtIndex:(NSInteger)index {
    
    return 1.0;
    
}

- (NSString *)titleForBarAtIndex:(NSInteger)index {
    
    // return [NSString stringWithFormat:@"%ld", index];
    switch (index){
    
        case 0:
            return @"fat";
        case 1:
            return @"fat";
        case 2:
            return @"protein";
        case 3:
            return @"protein";
    }
    
    return @"hej";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
