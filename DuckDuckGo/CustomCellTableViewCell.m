//
//  CustomCellTableViewCell.m
//  DuckDuckGo
//
//  Created by Jonas on 2015-03-16.
//  Copyright (c) 2015 Jonas. All rights reserved.
//

#import "CustomCellTableViewCell.h"

@implementation CustomCellTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
