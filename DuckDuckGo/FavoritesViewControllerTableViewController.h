//
//  FavoritesViewControllerTableViewController.h
//  DuckDuckGo
//
//  Created by Jonas on 2015-03-19.
//  Copyright (c) 2015 Jonas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavoritesViewControllerTableViewController : UITableViewController

@end
