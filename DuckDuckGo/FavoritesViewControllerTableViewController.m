//
//  FavoritesViewControllerTableViewController.m
//  DuckDuckGo
//
//  Created by Jonas on 2015-03-19.
//  Copyright (c) 2015 Jonas. All rights reserved.
//

#import "FavoritesViewControllerTableViewController.h"
#import "detailView.h"

@interface FavoritesViewControllerTableViewController ()
@property (nonatomic) NSMutableArray *arrayWithFoodNumbers;
@property (nonatomic) int intToPass;

@end

@implementation FavoritesViewControllerTableViewController

NSArray *arrayFavorites;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.arrayWithFoodNumbers = [[NSMutableArray alloc]init];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    arrayFavorites = [defaults objectForKey:@"favorites"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews {
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return arrayFavorites.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"theCell" forIndexPath:indexPath];
    
    // Configure the cell...
    
    [self.arrayWithFoodNumbers addObject:arrayFavorites[indexPath.row]];
    NSLog(@"indexpath: %ld", (long)indexPath.row);
    
    
    NSString *searchString = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%@", arrayFavorites[indexPath.row] ];
    NSURL *url =[NSURL URLWithString:searchString];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                
                                                if(error) {
                                                    NSLog(@"error in response: %@", error);
                                                    return;
                                                }
                                                
                                                NSError *parsingError = nil;
                                                
                                                NSDictionary *root = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:
                                                                      &parsingError];
                                                
                                                if (!parsingError) {
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        if (root.count > 0) {
                                                            
                                                            // NSLog(@"Title: %@", [root objectForKey:@"name"]);
                                                            cell.textLabel.text = [root objectForKey:@"name"];
                                                        } else {
                                                            
                                                            NSLog(@"No info found");
                                                            
                                                        }
                                                        
                                                    });
                                                    
                                                    
                                                } else {
                                                    
                                                    NSLog(@"Couldnot parse json: %@", parsingError);
                                                    
                                                }
                                                
                                            }];
    
    [task resume];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath
                                                                    *)indexPath {
    NSNumber *numberToPass = self.arrayWithFoodNumbers[indexPath.row];
    self.intToPass = [numberToPass intValue];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self performSegueWithIdentifier:@"detailSegueFromFavorites" sender:self];
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"detailSegueFromFavorites"]) {
        
        NSLog(@"intToPass: %d", self.intToPass);
        detailView *myVC = [segue destinationViewController];
        myVC.receivedInt = self.intToPass;
        
    }
    
    
}


@end
