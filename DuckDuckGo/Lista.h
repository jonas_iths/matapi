//
//  Lista.h
//  DuckDuckGo
//
//  Created by Jonas on 2015-02-23.
//  Copyright (c) 2015 Jonas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Lista : UITableViewController <UISearchBarDelegate>

@property(nonatomic) NSString *string;
@property(nonatomic) NSArray *array;

@end
