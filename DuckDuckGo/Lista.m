//
//  Lista.m
//  DuckDuckGo
//
//  Created by Jonas on 2015-02-23.
//  Copyright (c) 2015 Jonas. All rights reserved.
//

#import "Lista.h"
#import "detailView.h"
#import "CustomCellTableViewCell.h"

@interface Lista ()

@property (nonatomic) int intToPass;
@property (nonatomic) float height;
@property (strong, nonatomic) CustomCellTableViewCell *customCell;

@end

@implementation Lista

@synthesize array;


- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 55;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // NSLog(@"Nummertest: %@", self.string);
    // NSLog(@"Array length: %d", (int)array.count);

    // NSString *stringToLog = [array[4] objectForKey:@"name"];
    // NSLog(@"%@", stringToLog);
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath
                                                                    *)indexPath {
    
    self.intToPass = [[array[indexPath.row] objectForKey:@"number"] intValue];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self performSegueWithIdentifier:@"detailSegue" sender:self];

}
    
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CustomCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"myCell" forIndexPath:indexPath];
    cell.labelWidth.constant = self.view.frame.size.width - 20;
    cell.CellTitle.text = [array[indexPath.row] objectForKey:@"name"];
    
    // set Energy value
    
    NSString *searchString = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%d", [[array[indexPath.row] objectForKey:@"number"] intValue]];
    NSURL *url =[NSURL URLWithString:searchString];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                
                                                NSError *parsingError = nil;
                                                
                                                NSDictionary *root = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:
                                                                &parsingError];
                                                
                                                if (!parsingError) {
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                       NSDictionary *dict = [root objectForKey:@"nutrientValues"];
                                                        
                                                       NSInteger energy = [(NSNumber *)[dict objectForKey:@"energyKj"] integerValue];
                                                        
                                                       cell.energyValue.text = [NSString stringWithFormat: @"Energivärde: %ld", (long)energy];
                                                        
                                                        // [self.tableView reloadData];
                                                        
                                                    });
                                                    
                                                } else {
                                                    
                                                    NSLog(@"Couldnot parse json: %@", parsingError);
                                                    
                                                }
                                                
                                            }];
    
    [task resume];

    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Calculate a height based on a cell
    if(!self.customCell) {
            
        self.customCell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
        
    }
    
    // Configure the cell
    self.customCell.CellTitle.text = [array[indexPath.row] objectForKey:@"name"];
    self.customCell.CellTitle.font = [UIFont fontWithName:@"Helvetica" size:17];
    self.customCell.labelWidth.constant = self.view.frame.size.width - 20;
    
    // Layout the cell
    [self.customCell layoutIfNeeded];
    
    // Get the height for the cell
    CGFloat height = [self.customCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    
    return height + 1;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewRowAction *a = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Add to favorites?" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        
        self.intToPass = [[array[indexPath.row] objectForKey:@"number"] intValue];
        [self saveFavorite];
        
    }];
    
    a.backgroundColor = [UIColor blueColor];
    
    return @[a];
    
}

- (void) saveFavorite {
    
    NSNumber *numberToSave = [NSNumber numberWithInt:self.intToPass];
    NSLog(@"number to save: %@", numberToSave);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *arrayFavorites = [[defaults objectForKey:@"favorites"]mutableCopy];
    
    NSLog(@"sise of array: %lu", arrayFavorites.count);
    
    if (arrayFavorites) {
        
        // check if in array already
        BOOL inArray = NO;
        for(int i=0; i < arrayFavorites.count; i++) {
            
            if (arrayFavorites[i] == numberToSave) {
                inArray = YES;
                break;
            }
            
        }
        
        if (inArray) {
            
           //  NSLog(@"already in array");
            
        } else {
            
            // NSLog(@"array exists but not with our number, lets add");
            [arrayFavorites addObject:numberToSave];
        
        }
        
    } else {
        
        // NSLog(@"empty array lets add");
        arrayFavorites = [[NSMutableArray alloc] init];
        [arrayFavorites addObject:numberToSave];
    }
    
    [defaults setObject:arrayFavorites forKey:@"favorites"];
    [defaults synchronize];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Favorite added!"
                                                   message: @""
                                                  delegate: self
                                         cancelButtonTitle:@"Ok"
                                         otherButtonTitles:nil];
    // [alert setTag:1];
    [alert show];
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        // ok actions........
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    
    [self searchMethod:searchBar.text];
    
}

- (void) searchMethod:(NSString*)searchString {

    
    searchString = [NSString stringWithFormat:@"http://matapi.se/foodstuff?query=%@",searchString];
    NSURL *url =[NSURL URLWithString:searchString];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                
                                                // NSLog(@"Completed! Data: %@, error: %@", data, error);
                                                
                                                if(error) {
                                                    NSLog(@"error in response: %@", error);
                                                    return;
                                                }
                                                
                                                NSError *parsingError = nil;
                                                
                                                NSArray *root = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:
                                                                 &parsingError];
                                                
                                                if (!parsingError) {
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                    
                                                        if (root.count > 0) {
                                                            
                                                            self.array = root;
                                                            
                                                        } else {
                                                            
                                                            self.array = @[@"No topics found"];
                                                            
                                                        }
                                                        
                                                        [self.tableView reloadData];
                                                        
                                                    });
                                                    
                                                } else {
                                                    
                                                    NSLog(@"Couldnot parse json: %@", parsingError);
                                                    
                                                }
                                                
                                            }];
    
    [task resume];
    [self.view endEditing:YES];

}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"detailSegue"]) {
        
        detailView *myVC = [segue destinationViewController];
        myVC.receivedInt = self.intToPass;
        
    }
    
}

@end
