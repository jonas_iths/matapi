//
//  MenyController.m
//  DuckDuckGo
//
//  Created by Jonas on 2015-03-16.
//  Copyright (c) 2015 Jonas. All rights reserved.
//

#import "MenyController.h"
#import "CompareViewController.h"

@interface MenyController ()

@property (weak, nonatomic) IBOutlet UIButton *SearchButton;
@property (weak, nonatomic) IBOutlet UIButton *FavoriteButton;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UIButton *compare;
@property(nonatomic) NSString *randomFoodName1;
@property(nonatomic) NSString *randomFoodName2;
@property(nonatomic) NSMutableArray *compareArray;

@end

@implementation MenyController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.compareArray = [[NSMutableArray alloc]init];
    
}
- (IBAction)comparButton:(id)sender {
    
    [self compareTwoRandomFoodThings];
    
}


- (void)viewDidLayoutSubviews {

    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    CGPoint prePos = self.SearchButton.center;
    
    self.SearchButton.center = CGPointMake(prePos.x, 2000);
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationDelay:0.2];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    
    self.SearchButton.center = prePos;
    
    [UIView commitAnimations];
    
    
    prePos = self.FavoriteButton.center;
    
    self.FavoriteButton.center = CGPointMake(prePos.x, 2000);
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationDelay:0.5];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    
    self.FavoriteButton.center = prePos;
    
    [UIView commitAnimations];

    
    prePos = self.compare.center;
    
    self.compare.center = CGPointMake(prePos.x, 2000);
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationDelay:0.9];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    
    self.compare.center = prePos;
    
    [UIView commitAnimations];
    
    
    prePos = self.image.center;
    
    self.image.center = CGPointMake(prePos.x, 2000);
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationDelay:1.2];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    
    self.image.center = prePos;
    
    [UIView commitAnimations];
    
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"compareSegue"]) {
        
        CompareViewController *myVC = [segue destinationViewController];
        
        myVC.foodThing1 = self.randomFoodName1;
        myVC.foodThing2 = self.randomFoodName2;
        myVC.array = self.compareArray;
        
    }
}


- (void)compareTwoRandomFoodThings {
    
    int randomNumber = arc4random_uniform(999) + 1;
    
    NSString *searchString = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%d", randomNumber];
    NSURL *url =[NSURL URLWithString:searchString];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                
                                                NSError *parsingError = nil;
                                                
                                                NSDictionary *root = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:
                                                                      &parsingError];
                                                
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        self.randomFoodName1 = [root objectForKey:@"name"];
                                                        
                                                        NSDictionary *dict = [root objectForKey:@"nutrientValues"];

                                                        NSLog(@"COUNT: %lu", (unsigned long)[self.compareArray count]);
                                                        
                                                        if ([self.compareArray count] != 0) {
                                                            [self.compareArray removeAllObjects];
                                                            NSLog(@"COUNT: %lu", (unsigned long)[self.compareArray count]);
                                                        }
                                                        
                                                        if ([dict objectForKey:@"fat"] != nil) {
                                                            [self.compareArray  addObject:[dict objectForKey:@"fat"]];
                                                        } else {
                                                            [self.compareArray  addObject:@"0"];
                                                        }
                                                        
                                                        if ([dict objectForKey:@"protein"] != nil) {
                                                            [self.compareArray  addObject:[dict objectForKey:@"protein"]];
                                                        } else {
                                                            [self.compareArray  addObject:@"0"];
                                                        }
                                                        
                                                        [self compareTwoRandomFoodThings2];
                                                        
                                                    });
                                                
                                                
                                                
                                            }];
    
    [task resume];
    
    
}

- (void)compareTwoRandomFoodThings2 {
    
    int randomNumber = arc4random_uniform(999) + 1;
    
    NSString *searchString = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%d", randomNumber];
    NSURL *url =[NSURL URLWithString:searchString];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                
                                                NSError *parsingError = nil;
                                                
                                                NSDictionary *root = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:
                                                                      &parsingError];
                                                
                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                    
                                                    self.randomFoodName2 = [root objectForKey:@"name"];
                                                    
                                                    NSDictionary *dict = [root objectForKey:@"nutrientValues"];
                                                    
                                                    if ([dict objectForKey:@"fat"] != nil) {
                                                        [self.compareArray  addObject:[dict objectForKey:@"fat"]];
                                                    } else {
                                                        [self.compareArray  addObject:@"0"];
                                                    }
                                                    
                                                    if ([dict objectForKey:@"protein"] != nil) {
                                                        [self.compareArray  addObject:[dict objectForKey:@"protein"]];
                                                    } else {
                                                        [self.compareArray  addObject:@"0"];
                                                    }
                                                    
                                                    [self performSegueWithIdentifier:@"compareSegue" sender:self];
                                                    
                                                });
                                                
                                            }];
    
    [task resume];
    
    
}




@end
