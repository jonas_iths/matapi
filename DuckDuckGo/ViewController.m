//
//  ViewController.m
//  DuckDuckGo
//
//  Created by Jonas on 2015-02-23.
//  Copyright (c) 2015 Jonas. All rights reserved.
//

#import "ViewController.h"
#import "Lista.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *searchField;
@property (nonatomic) NSString *stringToPass;
@property (nonatomic) NSArray *arrayToPass;
@property (weak, nonatomic) IBOutlet UILabel *laddarLabel;

@end

@implementation ViewController


- (IBAction)onSearch:(id)sender {
    
    // Show load symbol
    self.laddarLabel.text = @"Laddar, vänligen vänta ett par timmar";
    
    NSString *searchString = [NSString stringWithFormat:@"http://matapi.se/foodstuff?query=%@",  self.searchField.text];
    NSURL *url =[NSURL URLWithString:searchString];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                
                                                if(error) {
                                                    NSLog(@"error in response: %@", error);
                                                    return;
                                                }
                                                
                                                NSError *parsingError = nil;
                                                
                                                NSArray *root = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error: &parsingError];
                                                
                                                if (root.count != 0) {
                                                
                                                    if (!parsingError) {
                                                        
                                                        NSDictionary *relatedTopics = root[0];
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            if (root.count > 0) {
                                                                
                                                                self.stringToPass = [relatedTopics objectForKey:@"name"];
                                                                self.arrayToPass = root;
                                                                
                                                                [self performSegueWithIdentifier:@"theSegue" sender:self];
                                                                
                                                            } else {
                                                                
                                                                NSLog(@"No topics found");
                                                                
                                                            }
                                                            
                                                        });
                                                        
                                                    } else {
                                                        
                                                        NSLog(@"Couldnot parse json: %@", parsingError);
                                                        
                                                    }
                                                
                                                } else {
                                                    
                                                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"No results found!"
                                                                                                   message: @""
                                                                                                  delegate: self
                                                                                         cancelButtonTitle:@"Ok"
                                                                                         otherButtonTitles:nil];
                                                    [alert show];
                                                
                                                    self.laddarLabel.text = @"";
                                                    
                                                }
                                            
                                            }];
    
    
    [task resume];
    [self.view endEditing:YES];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.laddarLabel.text = @"";
    
}

- (void)viewDidLayoutSubviews {
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"theSegue"]) {
        
        Lista *myVC = [segue destinationViewController];
        myVC.string = self.stringToPass;
        myVC.array = self.arrayToPass;
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
