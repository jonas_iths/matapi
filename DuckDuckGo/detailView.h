//
//  detailView.h
//  DuckDuckGo
//
//  Created by Jonas on 2015-02-25.
//  Copyright (c) 2015 Jonas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface detailView : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property(nonatomic) int receivedInt;

@end
