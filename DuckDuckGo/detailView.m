//
//  detailView.m
//  DuckDuckGo
//
//  Created by Jonas on 2015-02-25.
//  Copyright (c) 2015 Jonas. All rights reserved.
//

#import "detailView.h"

@interface detailView ()

@property (nonatomic) NSArray *array;
@property (nonatomic) NSDictionary *dictionary;


@property (weak, nonatomic) IBOutlet UITextView *textField;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint;

@property (weak, nonatomic) IBOutlet UIImageView *image;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageHeightConstraint;

@property (weak, nonatomic) IBOutlet UILabel *nyttighet;
@end

@implementation detailView

- (IBAction)buttonTakePicture:(id)sender {
    
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
}

- (void)saveImage: (UIImage*)image
{
    if (image != nil)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                             NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:
                          [NSString stringWithFormat:@"%d.png", self.receivedInt] ];
        NSData* data = UIImagePNGRepresentation(image);
        
        if (![data writeToFile:path atomically:YES]){
            NSLog(@"Failed to cache image to disk");
        } else {
            [data writeToFile:path atomically:YES];
            self.imageHeightConstraint.constant = 200;
        }
    }
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    // UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    self.image.image = chosenImage;
    
    int a = chosenImage.size.width;
    int b = chosenImage.size.height;
    
    NSLog(@"width: %d     height: %d",a, b);
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    // save image
    [self saveImage:chosenImage];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


- (void)loadImage
{
    NSString *imageName = [NSString stringWithFormat:@"%d.png", self.receivedInt];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                      imageName];
    
    UIImage *image = [UIImage imageWithContentsOfFile:path];
    
    if (image) {
        self.image.image = [UIImage imageWithContentsOfFile:path];
    } else {
        self.imageHeightConstraint.constant = 0;
    }
        
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    // NSLog(@"nummer: %d", self.receivedInt);
    [self searchMethod];
    
    [self loadImage];
    
    // self.scrollen.scrollEnabled = YES;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) searchMethod {
    
    
    NSString *searchString = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%d", self.receivedInt];
    NSURL *url =[NSURL URLWithString:searchString];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                
                                                if(error) {
                                                    NSLog(@"error in response: %@", error);
                                                    return;
                                                }
                                                
                                                NSError *parsingError = nil;
                                                
                                                NSDictionary *root = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:
                                                                 &parsingError];
                                                
                                                if (!parsingError) {
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        if (root.count > 0) {
                                                           
                                                            self.textField.text = @"";
                                                            
                                                           // set viewcontroller title
                                                            self.title = [root objectForKey:@"name"];
                                                            
                                                           // set textview with info
                                                           self.dictionary = [root objectForKey:@"nutrientValues"];
                                                           
                                                            float protein = 1111;
                                                            float water = 22222;
                                                            
                                                           for (id key in self.dictionary) {
                                                               // NSLog(@"%@", key);
                                                               
                                                               NSString *info = [self.dictionary objectForKey:key];
                                                               
                                                               if ( [key isEqualToString:@"protein"] ) {
                                                                   NSLog(@"PROETIN: %@", [self.dictionary objectForKey:key]);
                                                                   protein = [[self.dictionary objectForKey:key] floatValue];
                                                               }
                                                               if ( [key isEqualToString:@"water"] ) {
                                                                   water = [[self.dictionary objectForKey:key] floatValue];
                                                                   NSLog(@"water: %@", [self.dictionary objectForKey:key]);
                                                               }
                                                               
                                                               NSString *inTheTextfield = self.textField.text;
                                                               
                                                               self.textField.text = [NSString stringWithFormat:@"%@ %@: %@ \n", inTheTextfield, key, info];
                                                           
                                                           }
                                                           
                                                            self.nyttighet.text = [NSString stringWithFormat:@"Nyttighetsvärde: %.02f", protein * water];
                                                           
 
                                                            CGSize sizeThatShouldFitTheContent = [self.textField sizeThatFits:self.textField.frame.size];
                                                            self.constraint.constant = sizeThatShouldFitTheContent.height;
                                                            
                                                            
                                                            
                                                        } else {
                                                            
                                                           NSLog(@"No info found");
                                                            
                                                        }
                                                        
                                                    });
                                                    
                                                    
                                                } else {
                                                    
                                                    NSLog(@"Couldnot parse json: %@", parsingError);
                                                    
                                                }
                                                
                                            }];
    
    [task resume];    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
